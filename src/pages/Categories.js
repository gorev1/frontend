import React, { useState } from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse,
  makeStyles,
} from '@mui/material';
import { ExpandLess, ExpandMore } from '@mui/icons-material';

const categories = [
  { id: 1, name: 'c1', parentId: null },
  { id: 2, name: 'c2', parentId: 1 },
  { id: 3, name: 'c3', parentId: null },
  { id: 4, name: 'c4', parentId: 2 },
  { id: 5, name: 'c5', parentId: 2 },
  { id: 6, name: 'c6', parentId: 3 },
];

// const useStyles = makeStyles((theme) => ({
//   nested: {
//     paddingLeft: theme.spacing(4),
//   },
// }));

const CategoryRow = ({ category, children }) => {
  // const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const hasChildren = children && children.length > 0;

  return (
    <>
      <ListItem button onClick={handleToggle} className={category.parentId}>
        {hasChildren ? (
          open ? (
            <ListItemIcon>
              <ExpandLess />
            </ListItemIcon>
          ) : (
            <ListItemIcon>
              <ExpandMore />
            </ListItemIcon>
          )
        ) : (
          <ListItemIcon />
        )}
        <ListItemText primary={category.name} />
      </ListItem>
      {hasChildren && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          {children}
        </Collapse>
      )}
    </>
  );
};


const CategoriesPage = () => {
  const renderCategory = (category) => {
    const children = categories
      .filter((c) => c.parentId === category.id)
      .map((c) => renderCategory(c));
    return (
      <CategoryRow key={category.id} category={category}>
        {children}
      </CategoryRow>
    );
  };

  const rootCategories = categories.filter((c) => c.parentId === null);
  const categoryRows = rootCategories.map((c) => renderCategory(c));

  return (
    <List component="nav" aria-labelledby="nested-list-subheader">
      {categoryRows}
    </List>
  );
};

export default CategoriesPage;
