import React, { useState } from 'react';
import {
  Button,
  FormControl,
  InputLabel,
  ListItemButton,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';
import HeaderApp from '../components/HeaderApp.js';
import ItemList from '../ItemList.js';
import Page from '../components/Page.js';
import ItemListTable from '../components/ItemListTable.js';
import CreateUserForm from '../components/CreateUser.js';

function CreateUserPage() {
  return (
    <Page el={CreateUserForm} />
  );
}

export default CreateUserPage;
