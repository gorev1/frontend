import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { ArrowDropUp, ArrowDropDown, ArrowDownward } from '@mui/icons-material';
import ItemForm from '../components/ItemForm';
import axios from "axios";
import { Navigate, useNavigate } from 'react-router-dom';
import ActionHistory from '../components/ItemHistory';
import Page from '../components/Page';
import { BACKEND_URL } from '../constants';

const ItemPage = () => {
  const { id } = useParams();
  // const history = useHistory();
  const [item, setItem] = useState({});
  const urlTail = window.location.pathname.replace(/\/+$/, '').split('/').at(-1);
  const isUpdate = urlTail === 'update';
  const isCreate = urlTail === 'create';
  const isReadOnly = !(isUpdate || isCreate);

  let navigate = useNavigate();
  const fetchData = async (id) => {
    try {
      const res = await axios.get(`${BACKEND_URL}/api/item/${id}`);
      setItem(res.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (id && !['create', 'update'].includes(id)) {
      const res = fetchData(id);
      setItem(res.data);
    }
  }, [id]);

  const onUpdate = async (formFields) => {
    try {
      const res = await axios.put(`${BACKEND_URL}/api/item/${id}`, formFields);
      setItem(res.data);
      navigate(`/item/${item.id}`)
    } catch (error) {
      // handle the error
    }
  };

  const onCreate = async (formFields) => {
    try {
      const res = await axios.post(`${BACKEND_URL}/api/item`, formFields);
      navigate(`/item/${res.data.id}`);
    } catch (error) {
      // handle the error
    }
  };

  const handleSave = (updatedItem) => {
    fetch(`${BACKEND_URL}/api/item/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedItem)
    })
      .then(response => response.json())
      .then(data => setItem(data));
  };

  const component = () => {
    return <>
        {item ? (
          <>
            <ItemForm item={item} isReadOnly={isReadOnly} onSubmit={isCreate ? onCreate : onUpdate} />
            {isReadOnly && <ActionHistory item={item} />}
          </>
        ) : (
          <div>Loading...</div>
        )}
      </>
  }

  return (
    <Page el={component} />
  );
};

export default ItemPage;
