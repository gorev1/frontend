import React, { useState, useEffect } from "react";
import {
    Button,
    FormControl,
    InputLabel,
    ListItemButton,
    MenuItem,
    Select,
    TextField,
} from '@mui/material';
import axios from "axios";
import { BACKEND_URL } from "./constants";

const validOrders = ["name", "-name", "path", "-path"];

const ItemList = () => {
    const [order, setOrder] = useState("name");
    const [currentPage, setCurrentPage] = useState(1);
    const [nameLike, setNameLike] = useState("");
    const [categoryLike, setCategoryLike] = useState("");
    const [countGe, setCountGe] = useState("");
    const [countLe, setCountLe] = useState("");
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);
    const [pageButtons, setPageButtons] = useState([]);

    const fetchData = async (pageNumber) => {
        const params = {};
        if (order) {
            params.order = order;
        }
        if (pageNumber) {
            params.page = pageNumber;
        } else {
            params.page = currentPage;
        }
        if (nameLike) {
            params.name__like = nameLike;
        }
        if (categoryLike) {
            params.category__like = categoryLike;
        }
        if (countGe) {
            params.count__ge = countGe;
        }
        if (countLe) {
            params.count__le = countLe;
        }

        try {
            const res = await axios.get(`${BACKEND_URL}/api/item`, {
                params
            });
            setItems(res.data.items);
            setTotal(res.data.total);
            updatePageButtons(res.data.total);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData();
    }, [order, currentPage, nameLike, categoryLike, countGe, countLe]);

    const handleOrderClick = () => {
        const currentIndex = validOrders.indexOf(order);
        const nextIndex = (currentIndex + 1) % validOrders.length;
        setOrder(validOrders[nextIndex]);
        setCurrentPage(1);
    };

    const handleNameLikeChange = (event) => {
        setNameLike(event.target.value);
        setCurrentPage(1);
    };

    const handleCategoryLikeChange = (event) => {
        setCategoryLike(event.target.value);
        setCurrentPage(1);
    };

    const handleCountGeChange = (event) => {
        setCountGe(event.target.value);
        setCurrentPage(1);
    };

    const handleCountLeChange = (event) => {
        setCountLe(event.target.value);
        setCurrentPage(1);
    };

    const handlePageClick = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const updatePageButtons = (total) => {
        const totalPages = Math.max(Math.ceil(total / 10), 1);
        const _pageButtons = [];
        for (let i = 1; i <= totalPages; i++) {
            _pageButtons.push(
                <Button
                    key={i}
                    onClick={() => handlePageClick(i)}
                    variant={currentPage === i ? "contained" : "text"}
                >
                    {i}
                </Button>
            );
        }
        setPageButtons(_pageButtons);
    }

    return (
        <>
            <div>
                <Button onClick={handleOrderClick}>
                    {order === "name" ? "Sort by Name" :
                        order === "-name" ? "Sort by Name (Descending)" :
                            order === "path" ? "Sort by Path" :
                                order === "-path" ? "Sort by Path (Descending)" : "Unknown Order"}
                </Button>
            </div>
            <div>
                <TextField
                    label="Name contains"
                    value={nameLike}
                    onChange={handleNameLikeChange}
                />
                <TextField
                    label="Category contains"
                    value={categoryLike}
                    onChange={handleCategoryLikeChange}
                />
                <TextField
                    label="Count greater than or equal to"
                    value={countGe}
                    onChange={handleCountGeChange}
                />
                <TextField
                    label="Count less than or equal to"
                    value={countLe}
                    onChange={handleCountLeChange}
                />
            </div>
            <div>
                {items.map((item) => (
                    <ListItemButton key={item.id}>
                        {item.name}
                    </ListItemButton>
                ))}
            </div>
            <div>
                {pageButtons}
            </div>
        </>
    );
};

export default ItemList;
