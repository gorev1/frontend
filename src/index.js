import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import HomePage from './pages/Home';
import reportWebVitals from './reportWebVitals';
import {Routes, Route, createBrowserRouter, RouterProvider} from 'react-router-dom';
import CategoriesPage from './pages/Categories';
import ItemPage from './pages/Item';
import CreateUserPage from './pages/CreateUser';

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/item/create",
    element: <ItemPage />,
  },
  {
    path: "/item/:id/update",
    element: <ItemPage />,
  },
  {
    path: "/item/:id",
    element: <ItemPage />,
  },
  {
    path: "/user/create",
    element: <CreateUserPage />,
  },
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
