import { useState } from 'react';
import { TextField, Button, FormControl, Select, MenuItem, InputLabel } from '@mui/material';
import axios from 'axios';
import { BACKEND_URL } from '../constants';

const CreateUserForm = () => {
    const [userData, setUserData] = useState({
        fio: '',
        email: '',
        password: '',
        phone_number: '',
        status: '',
        position: '',
        role: 'admin'
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUserData(prevData => ({ ...prevData, [name]: value }));
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await axios.post(`${BACKEND_URL}/api/user`, userData);
            setUserData({
                fio: '',
                email: '',
                password: '',
                phone_number: '',
                status: '',
                position: '',
                role: 'admin'
            });
        } catch (error) {
            alert(error.response.data.message)
            console.error(error);
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <TextField
                label="Full Name"
                name="fio"
                value={userData.fio}
                onChange={handleChange}
                required
                fullWidth
                margin="normal"
            />
            <TextField
                label="Email"
                name="email"
                value={userData.email}
                onChange={handleChange}
                required
                fullWidth
                margin="normal"
            />
            <TextField
                label="Password"
                type="password"
                name="password"
                value={userData.password}
                onChange={handleChange}
                required
                fullWidth
                margin="normal"
            />
            <TextField
                label="Phone Number"
                name="phone_number"
                value={userData.phone_number}
                onChange={handleChange}
                fullWidth
                margin="normal"
            />
            <FormControl fullWidth margin="normal">
                <InputLabel>Status</InputLabel>
                <Select
                    name="status"
                    value={userData.status}
                    onChange={handleChange}
                    required
                >
                    <MenuItem value="active">Active</MenuItem>
                    <MenuItem value="inactive">Inactive</MenuItem>
                </Select>
            </FormControl>
            <TextField
                label="Position"
                name="position"
                value={userData.position}
                onChange={handleChange}
                fullWidth
                margin="normal"
            />
            <FormControl fullWidth margin="normal">
                <InputLabel>Role</InputLabel>
                <Select
                    name="role"
                    value={userData.role}
                    onChange={handleChange}
                    required
                >
                    <MenuItem value="admin">admin</MenuItem>
                    <MenuItem value="user">user</MenuItem>
                </Select>
            </FormControl>
            <Button type="submit" variant="contained" color="primary">
                Create User
            </Button>
        </form>
    );
};

export default CreateUserForm;
