import { useState } from 'react';
import { Box, TextField, Button } from '@mui/material';
import { Navigate, useNavigate } from 'react-router-dom';

function ItemForm({ item, isReadOnly, onSubmit }) {
  let _isReadOnly
  const [formFields, setFormFields] = useState(item);
  let navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormFields({ ...formFields, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit(formFields);
  };

  const onSave = () => {
    // onSubmit(formFields);
  }
  const onCancel = () => {
    navigate(-1);
    setFormFields(item);
  }

  console.log(item)
  return (
    <>
      <form onSubmit={handleSubmit}>
        <TextField
          name="name"
          label="Name"
          value={formFields.name}
          onChange={handleChange}
          fullWidth
          margin="normal"
          required
          InputProps={{
            readOnly: isReadOnly,
          }}
        />
        <TextField
          name="description"
          label="Description"
          value={formFields.description}
          onChange={handleChange}
          fullWidth
          margin="normal"
          multiline
          rows={4}
          InputProps={{
            readOnly: isReadOnly,
          }}
        />
        <TextField
          name="category_name"
          label="Category"
          value={formFields.category_name}
          onChange={handleChange}
          fullWidth
          margin="normal"
          required
          InputProps={{
            readOnly: isReadOnly,
          }}
        />
        <TextField
          name="count"
          label="Count"
          value={formFields.count}
          onChange={handleChange}
          margin="normal"
          required
          InputProps={{
            readOnly: isReadOnly,
          }}
        />

        <br />
        {!isReadOnly &&
          <Button type="submit" variant="contained" color="primary" onClick={onSave}>
            Save
          </Button>
        }
        {isReadOnly && <Button variant="contained" color="primary" onClick={() => navigate(`/item/${item.id}/update`)}>
          Update
        </Button>}
        {!isReadOnly && <Button variant="contained" color="primary" onClick={onCancel} sx={{ 'place': 'flex' }}>
          Cancel
        </Button>}
      </form>
    </>
  );
}

export default ItemForm;
