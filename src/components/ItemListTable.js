import { useState, useEffect } from "react";
import { Button, TextField, Table, TableHead, TableBody, TableRow, TableCell, IconButton } from "@mui/material";
import { ArrowDropUp, ArrowDropDown, ArrowDownward } from '@mui/icons-material';
import axios from "axios";
import ClickableRow from "../Clickable";
import styled from 'styled-components';
import { Navigate, useNavigate } from 'react-router-dom';
import { BACKEND_URL } from '../constants';

const validOrders = ["name", "-name", "path", "-path"];

const StyledButton = styled(Button)`
&& {
    background-color: #ffffff;
    border: 1px solid #3f51b5;
    color: #3f51b5;
    font-weight: bold;
    text-transform: uppercase;
    font-size: 12px;
    &:hover {
        background-color: #3f51b5;
        color: #ffffff;
    }
}
`;

const columns = [
    { name: 'Id', field: 'id' },
    { name: 'Name', field: 'name' },
    { name: 'Category', field: 'category' },
    { name: 'Count', field: 'count' },
];

const ItemListTable = () => {
    const [order, setOrder] = useState("name");
    const [currentPage, setCurrentPage] = useState(1);
    const [nameLike, setNameLike] = useState("");
    const [categoryLike, setCategoryLike] = useState("");
    const [countGe, setCountGe] = useState("");
    const [countLe, setCountLe] = useState("");
    const [items, setItems] = useState([]);
    const [total, setTotal] = useState(0);
    const [pageButtons, setPageButtons] = useState([]);


    let navigate = useNavigate();
    function OpenItemPage({ item }) {
        navigate(`/item/${item.id}`);
    }

    const fetchData = async (pageNumber) => {
        const params = {};
        if (order) {
            params.order = order;
        }
        if (pageNumber) {
            params.page = pageNumber;
        } else {
            params.page = currentPage;
        }
        if (nameLike) {
            params.name__like = nameLike;
        }
        if (categoryLike) {
            params.category__like = categoryLike;
        }
        if (countGe) {
            params.count__ge = countGe;
        }
        if (countLe) {
            params.count__le = countLe;
        }
        try {
            const res = await axios.get(`${BACKEND_URL}/api/item`, {
                params: params,
            });
            setItems(res.data.items);
            setTotal(res.data.total);
            updatePageButtons(res.data.total);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData();
    }, [order, currentPage, nameLike, categoryLike, countGe, countLe]);

    const handleOrderClick = (newOrder) => {
        setOrder(newOrder);
        setCurrentPage(1);
    };

    const handleNameLikeChange = (event) => {
        setNameLike(event.target.value);
        setCurrentPage(1);
    };

    const handleCategoryLikeChange = (event) => {
        setCategoryLike(event.target.value);
        setCurrentPage(1);
    };

    const handleCountGeChange = (event) => {
        setCountGe(event.target.value);
        setCurrentPage(1);
    };

    const handleCountLeChange = (event) => {
        setCountLe(event.target.value);
        setCurrentPage(1);
    };

    const handlePageClick = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleItemClick = (event) => {
    };

    const updatePageButtons = (total) => {
        const totalPages = Math.max(Math.ceil(total / 10), 1);
        const _pageButtons = [];
        for (let i = 1; i <= totalPages; i++) {
            _pageButtons.push(
                <Button
                    key={i}
                    onClick={() => handlePageClick(i)}
                    variant={currentPage === i ? "contained" : "text"}
                >
                    {i}
                </Button>
            );
        }
        setPageButtons(_pageButtons);
    }
    return (
        <div>
            <div>
                <div style={{ marginBottom: '10px' }} />
                <TextField label="Name" value={nameLike} onChange={handleNameLikeChange} />
                <TextField label="Category" value={categoryLike} onChange={handleCategoryLikeChange} />
                <TextField label="Count min" value={countGe} onChange={handleCountGeChange} />
                <TextField label="Count max" value={countLe} onChange={handleCountLeChange} />
            </div>
            <Table sx={{ minWidth: 650 }}>
                <ItemTableHeader
                    columns={columns}
                    order={order}
                    onOrderChange={handleOrderClick}
                />
                <TableBody>
                    {items.map((item) => (
                        <ClickableRow
                            key={item.id}
                            onClick={() => OpenItemPage({item})}
                        >
                            <TableCell>{item.id}</TableCell>
                            <TableCell>{item.name}</TableCell>
                            <TableCell>{item.category_name}</TableCell>
                            <TableCell>{item.count}</TableCell>
                        </ClickableRow>
                    ))}
                </TableBody>
            </Table>
            <div>{pageButtons}</div>
        </div>
    );
};

export default ItemListTable;


function ItemTableHeader({ columns, order, onOrderChange }) {
    // Define a function to handle column clicks
    const handleColumnClick = (field) => {
        // Update the order state based on the current order and the clicked column
        if (field === order.replace(/^-/, '')) {
            // If the same column was clicked, toggle the order direction
            onOrderChange(order.startsWith('-') ? field : `-${field}`);
        } else {
            // If a different column was clicked, reset the order direction to ascending
            onOrderChange(field);
        }
    };

    return (
        <TableHead>
            <TableRow>
                {columns.map(({ name, field }) => (
                    <TableCell key={field}>
                        {name}
                        <IconButton
                            onClick={() => handleColumnClick(field)}
                            color={order.endsWith(field) ? 'primary' : 'default'}
                        >
                            {order.endsWith(field) && order.startsWith('-') ? <ArrowDropDown /> : <ArrowDropUp />}
                        </IconButton>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}
