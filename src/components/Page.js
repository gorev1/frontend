import React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import HeaderApp from './HeaderApp';

const Page = ({ el: Component }) => {
    return (
        <React.Fragment>
            <AppBar position="static">
                <Toolbar>
                    <HeaderApp />
                </Toolbar>
            </AppBar>
            <Component />
        </React.Fragment>
    );
};

export default Page;