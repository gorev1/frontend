import React, { useEffect, useState } from 'react';
import {
  AppBar,
  Box,
  Button,
  FormControl,
  IconButton,
  InputLabel,
  ListItemButton,
  ListItemText,
  MenuItem,
  Select,
  TextField,
  Toolbar,
  Modal,
  Typography,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import AddIcon from '@mui/icons-material/Add';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import { Logout } from '@mui/icons-material';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { BACKEND_URL } from '../constants';

export default function HeaderApp(props) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      {/* <AppBar position="static"> */}
      <Toolbar>
        <Typography variant="h6" component={Link} to={"/"} sx={{ textDecoration: 'none' }} color={'inherit'}>
          Хранилище Aivok
        </Typography>
        <div style={{ flexGrow: 1 }}></div>
        <LoginModal />
      </Toolbar>
    </Box>
  );
}


const LoginModal = () => {
  const [open, setOpen] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [authUser, setAuthUser] = useState(null);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res = await axios.post(`${BACKEND_URL}/auth/login`, {
        email: email,
        password: password,
      });
      localStorage.setItem('auth', JSON.stringify(res.data));
      setAuthUser(res.data);
      handleClose();
    } catch (error) {
      alert(error.response.data.detail);
    }
  };

  const onLogout = () => {
    localStorage.removeItem('auth');
    setAuthUser(null);
  }

  useEffect(() => {
    setAuthUser(JSON.parse(localStorage.getItem('auth')));
  }, []);


  if (!authUser) return (
    <>
      <Button variant="contained" onClick={handleOpen}>Login</Button>
      <Modal open={open} onClose={handleClose}>
        <div style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', backgroundColor: '#fff', padding: '20px', outline: 'none' }}>
          <h2>Авторизация</h2>
          <form onSubmit={handleSubmit}>
            <TextField label="Email" variant="outlined" value={email} onChange={(e) => setEmail(e.target.value)} style={{ marginBottom: '10px' }} />
            <br />
            <TextField label="Password" type="password" variant="outlined" value={password} onChange={(e) => setPassword(e.target.value)} style={{ marginBottom: '10px' }} />
            <br /><br />
            <Button type="submit" variant="contained" color="primary">Login</Button>
          </form>
        </div>
      </Modal>
    </>
  );

  return (
    <>
      {authUser.role === 'admin' && <AdminButtons />}
      <Typography variant='h8'>
        {`Вы вошли как: ${authUser.fio}`}
      </Typography>
      <div style={{ marginLeft: 10 }} />
      <Button variant="contained" onClick={onLogout}>
        <Logout />
      </Button>
    </>
  )
};

const AdminButtons = () => {
  const navigate = useNavigate();

  const handleCreateItem = () => {
    navigate('/item/create');
  };

  const handleCreateUser = () => {
    navigate('/user/create');
  };

  return (
    <div>
      <Button variant="contained" startIcon={<AddIcon />} onClick={handleCreateItem}>
        Добавить запись
      </Button>
      <Button variant="contained" startIcon={<PersonAddIcon />} onClick={handleCreateUser}>
        Добавить пользователя
      </Button>
    </div>
  );
};
