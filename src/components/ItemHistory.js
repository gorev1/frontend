import React, { useEffect, useState } from "react";
import { Box, Button, Grid, MenuItem, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import { Add as AddIcon, History as HistoryIcon } from "@mui/icons-material";
import axios from "axios";
import { BACKEND_URL } from "../constants";

const ActionHistory = ({ item }) => {
  const [pageButtons, setPageButtons] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [orders, setOrders] = useState([]);
  const [orderForm, setOrderForm] = useState({ how_much: 1, type: "Взять" });

  const handlePageClick = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const fetchData = async (pageNumber) => {
    try {
      const res = await axios.get(`${BACKEND_URL}/api/order`, {
        params: { item_id: item.id, page: pageNumber },
      });
      setOrders(res.data.items);
      updatePageButtons(res.data.total);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData(currentPage);
  }, [currentPage]);

  const updatePageButtons = (total) => {
    const totalPages = Math.max(Math.ceil(total / 10), 1);
    const _pageButtons = [];
    for (let i = 1; i <= totalPages; i++) {
      _pageButtons.push(
        <Button
          key={i}
          onClick={() => handlePageClick(i)}
          variant={currentPage === i ? "contained" : "text"}
        >
          {i}
        </Button>
      );
    }
    setPageButtons(_pageButtons);
  };

  const handleAddAction = () => {
    // const newOrder = { who: "New User", when: "2022-05-07", howMany: 1, type: "take" };
    const authUser = JSON.parse(localStorage.getItem("auth"));
    if (authUser === null) {
      alert("Вы не авторизованы");
      return;
    };
    try {
      const res = axios.post(`${BACKEND_URL}/api/order`, {
        item_id: item.id,
        user_id: authUser.id,
        date: new Date().toISOString(),
        how_much: orderForm.how_much,
        type: orderForm.type,
      });
      updatePageButtons(res.data.total);
    } catch (error) {
      console.error(error);
    }
    fetchData(1);
    setCurrentPage(1);
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setOrderForm({ ...orderForm, [name]: value });
  };

  return (
    <Box>
      <Typography variant="h6">{`История действий:`}</Typography>
      {/* <Button variant="contained" startIcon={<AddIcon />} onClick={handleAddAction}>
        Взять/вернуть
      </Button> */}

      <Box>
        <Grid container spacing={2} alignItems="center">

          <Grid item>
            <TextField name="how_much" label="Сколько" value={orderForm.how_much} onChange={handleChange}/>
          </Grid>
          <Grid item>
            <TextField name="type" label="Действие" select value={orderForm.type} onChange={handleChange}>
              <MenuItem value="Взять">Взять</MenuItem>
              <MenuItem value="Вернуть">Вернуть</MenuItem>
            </TextField>
          </Grid>
          {/* <TextField label="Who" defaultValue="user" style={{ display: "none" }} value/>
        <TextField label="Date" defaultValue={new Date().toISOString()} style={{ display: "none" }} /> */}
          <Grid item>
            <Button variant="contained" color="primary" onClick={handleAddAction}>
              Выполнить действие
            </Button>
          </Grid>
        </Grid>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Кто</TableCell>
              <TableCell>Когда</TableCell>
              <TableCell>Сколько</TableCell>
              <TableCell>Тип операции</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orders.map((order, index) => (
              <TableRow key={index}>
                <TableCell>{order.user.fio}</TableCell>
                <TableCell>{order.date}</TableCell>
                <TableCell>{order.how_much}</TableCell>
                <TableCell>
                  {order.type === "Взять" ? (
                    <HistoryIcon color="primary" />
                  ) : (
                    <HistoryIcon color="secondary" />
                  )}
                  {order.type}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      {pageButtons}
    </Box>
  );
};

export default ActionHistory;
