import React from "react";
import styled from "styled-components";

const ClickableRow = styled.tr`
  cursor: pointer;
  &:hover {
    background-color: #f5f5f5;
  }
`;

const Clickable = ({ children, onClick }) => (
  <ClickableRow onClick={onClick}>{children}</ClickableRow>
);

export default Clickable;
